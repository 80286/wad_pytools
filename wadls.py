#!/usr/bin/python3
# -*- coding:utf-8 -*-
# 27.09.15, 80286

from wad import Wad

from signal import signal, SIGPIPE, SIG_DFL
import getopt
import sys


class WadLsCfg(object):
    _short_options = "hg:dup:mGqrv"
    _long_options = ["help", "group=", "decimal", "human-readable", "print=", "list-maps", "list-groups", "quiet", "raw", "verbose"]

    @staticmethod
    def printUsage(argv):
        print("Prints entries in WAD file.")
        print("Usage: {0} [options] <file1.wad> [file2.wad] ...".format(argv[0]))
        print("-p, --print [NAME]: Print content of selected lump.")
        print("-m, --list-maps: Select map lumps (determines by format of lump name).")
        print("-r, --raw: Print raw bytes of lump selected in -p/--print.")
        print("-g, --group [GROUP]: Print all entries in selected group.")
        print("-G, --list-groups: Print all group identifiers in input wad file.")
        print("-q, --quiet: Disable printing decorations/messages. Useful with -p/--print.")
        print("-d, --decimal: Print file offsets with base 10 (default: 16).")
        print("-v, --verbose: Enable verbose mode.")
        sys.exit(1)

    def __init__(self):
        self.group = ""
        self.decimal = False
        self.human = False
        self.printList = []
        self.quiet = False
        self.raw = False
        self.verbose = False
        self.listMaps = False
        self.listGroups = False
        self.args = []

    def buildFromArgv(self, argv):
        opts = None

        try:
            (opts, self.args) = getopt.getopt(argv[1:], self._short_options, self._long_options)
        except getopt.GetoptError as e:
            print(str(e))
            self.printUsage(argv)

        if len(argv) == 1:
            self.printUsage(argv)

        for (key, val) in opts:
            if key in ("-h", "--help"):
                self.printUsage(argv)
            elif key in ("-g", "--group="):
                self.group = val
            elif key in ("-d", "--decimal"):
                self.decimal = True
            elif key in ("-u", "--human-readable"):
                self.human = True
            elif key in ("-p", "--print"):
                self.printList.append(val)
            elif key in ("-m", "--print-maps"):
                self.listMaps = True
            elif key in ("-G", "--print-groups"):
                self.listGroups = True
            elif key in ("-q", "--quiet"):
                self.quiet = True
            elif key in ("-r", "--raw"):
                self.raw = True
            elif key in ("-v", "--verbose"):
                self.verbose = True

        self.offsetFmt = "%d" if self.decimal else "0x%08x"
        self.sizeFmt = "%8s" if self.human else "%8s"
        self.lumpFmt = "[%4d]: %10s {0} at {1}".format(self.sizeFmt, self.offsetFmt)
        
    def shouldPrintOnlySelected(self):
        return (len(self.printList) != 0)

    def isPrintAllowed(self):
        return (not self.quiet) and (not self.shouldPrintOnlySelected())

    def isLumpAllowedToPrint(self, lump):
        return self.printList and (lump.getZeroStrippedName() in self.printList)


class WadLs(object):
    @staticmethod
    def getShortStrBytesNum(amount):
        if amount >= 1000:
            return "%6.6f kB" % (amount / 1000.0)
        elif amount >= 1000000:
            return "%6.6f MB" % (amount / 1000000.0)
        else:
            return "%d B" % (amount)

    @staticmethod
    def getFieldWidth(fieldStrings):
        return max([len(field_string) for field_string in fieldStrings])

    @staticmethod
    def getFieldTitles(fieldStrings, fieldWidth=None):
        if not fieldWidth:
            fieldWidth = WadLs.getFieldWidth(fieldStrings) + 1

        return "|".join([s.ljust(fieldWidth, ".") for s in fieldStrings])

    @staticmethod
    def printBar(title, printTitle=False):
        sym = "="

        msg = "{0}:{1}{2}".format(title.rjust(8, sym), ("start" if printTitle else "end").ljust(5, sym), sym * 38)
        print(msg)

    @staticmethod
    def printDecoratedData(lumpName, lumpData, bar):
        print("{0}\n{1}\n{0}".format(lumpName.rjust(8, ".") + bar, lumpData))

    @staticmethod
    def printFormattedList(records, fieldStrings):
        fieldWidth = WadLs.getFieldWidth(fieldStrings) + 1

        print(WadLs.getFieldTitles(fieldStrings, fieldWidth))

        # Adjust format of tuple element to size of field:
        fieldFmt = "%{0}s".format(fieldWidth)

        # Print list of structures:
        for record in records:
            print(" ".join([fieldFmt % (str(field).rstrip("\0")) for field in record]))

    @staticmethod
    def printMapRecords(wad, lump, lumpData):
        # If lump has Hexen format:
        if lumpData[1]:
            fieldStrings = wad.maplumps[lump.name + "_Hexen"][2]
        else:
            if wad.maplumps[lump.name]:
                fieldStrings = wad.maplumps[lump.name][2]
            else:
                # REJECT/BEHAVIOR/BLOCKMAP
                print(lump.data)
                return

        WadLs.printFormattedList(lumpData[0], fieldStrings)

    @staticmethod
    def printPatchNames(patchNames):
        numOfPatches = len(patchNames)
        i = 0

        for i in range(numOfPatches):
            print("{0}: '{1}'".format(i, patchNames[i]))
            i += 1

    @staticmethod
    def printTextures(lumpData):
        for (texinfo, patches) in lumpData:
            n = len(patches)
            texStr = "[{0}]; Size: {1} x {2} px {3} patch{4}".format(texinfo[0].rstrip('\0'), texinfo[2], texinfo[3], n, "es:" if n != 1 else ":")

            print(texStr)
            for patch in patches:
                print("\tPatch {0} at ({1}, {2}).".format(patch[2], patch[0], patch[1]))

    @staticmethod
    def _printSummary(wad, cfg, directory):
        headFmt = "%s: %8s; numlumps: %d; infotableofs: {0}".format(cfg.offsetFmt)
        isPrintAllowed = (not cfg.quiet) and (not cfg.shouldPrintOnlySelected()) and (not cfg.listGroups)
        filename = wad.filename

        if not isPrintAllowed:
            return

        print(headFmt % (filename, wad.info[0], wad.info[1], wad.info[2]))
        msg = ""

        if cfg.group:
            msg = "{0}: Selected {1} lumps from '{2}' group:".format(filename, len(directory), cfg.group)
        else:
            msg = "{0}: All {1} lumps:".format(filename, len(directory))

        print(msg)

    @staticmethod
    def _printIndex(cfg, lump):
        if cfg.quiet:
            return

        if cfg.listGroups:
            group = lump.getMarkerInfo()

            if group:
                print("{0}: {1} ({2})".format(lump.index, group[0], lump.name))

            return

        shouldPrintIndex = (not cfg.shouldPrintOnlySelected()) and (not cfg.listMaps or lump.isMapName())
        if not shouldPrintIndex:
            return

        lumpSize = WadLs.getShortStrBytesNum(lump.size) if cfg.human else lump.size
        print(cfg.lumpFmt % (lump.index, lump.name.rstrip("\0"), lumpSize, lump.offset))

    @staticmethod
    def _printRawData(cfg, lump):
        rawData = lump.data.decode("utf-8")

        if cfg.quiet:
            # Print raw bytes:
            print(rawData)
        else:
            # Print raw bytes inside bordered area (for better readibility):
            WadLs.printBar(lump.name, True)
            print(rawData)
            WadLs.printBar(lump.name)

    @staticmethod
    def _tryToExtractAndPrintData(wad, lump):
        lumpData = wad.getMapLumpDataList(lump)

        if lumpData:
            WadLs.printBar(lump.name, True)
            WadLs.printMapRecords(wad, lump, lumpData)
            WadLs.printBar(lump.name)
        else:
            lumpData = wad.getOtherLumpData(lump)

            printFunc = WadLs.printPatchNames if lump.isPatchesLump() else WadLs.printTextures
            printFunc(lumpData)

    @staticmethod
    def main(argv):
        cfg = WadLsCfg()
        cfg.buildFromArgv(argv)

        for arg in cfg.args:
            wad = Wad(arg)
            wad.verbose = cfg.verbose
            wad.read(readData=False)

            directory = wad.getGroup(cfg.group) if cfg.group else wad.directory

            WadLs._printSummary(wad, cfg, directory)

            for lump in directory:
                WadLs._printIndex(cfg, lump)

                if cfg.isLumpAllowedToPrint(lump):
                    wad.readLump(lump)

                    if cfg.raw:
                        WadLs._printRawData(cfg, lump)
                    else:
                        WadLs._tryToExtractAndPrintData(wad, lump)
            wad.close()


if __name__ == '__main__':
    # Ignore "Broken Pipe (errno 32)" exception:
    # (Set SIG_DFL(Default behavior) for SIGPIPE signal)
    signal(SIGPIPE, SIG_DFL)

    WadLs.main(sys.argv)
