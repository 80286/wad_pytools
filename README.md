### Simple python scripts used for basic operations on WAD files


Listing content of wad:

```
$ ./wadls.py test.wad

test.wad:     PWAD; numlumps: 11; infotableofs: 0x00009464
test.wad: All 11 lumps:
[   0]:       E1M7        0 at 0x0000000c
[   1]:     THINGS     4830 at 0x0000000c
[   2]:   LINEDEFS     3346 at 0x000012ea
[   3]:   SIDEDEFS     9360 at 0x00001ffc
(...)
```

Listing all maps in wad:

```
$ ./wadls.py -m doom1.wad

doom1.wad:     IWAD; numlumps: 2045; infotableofs: 0x009e2e84
doom1.wad: All 2045 lumps:
[   6]:       E1M1        0 at 0x0001f878
[  17]:       E1M2        0 at 0x0002d220
[  28]:       E1M3        0 at 0x00049150
[  39]:       E1M4        0 at 0x000643bc
[  50]:       E1M5        0 at 0x00079a74
[  61]:       E1M6        0 at 0x0008f760
[  72]:       E1M7        0 at 0x000b4944
(...)
```

Listing all groups in wad (maps or items inside markers `X_START` / `X_END`):

```
$ ./wadls.py -G doom1.wad

6: E1M1 (E1M1)
17: E1M2 (E1M2)
28: E1M3 (E1M3)
39: E1M4 (E1M4)
50: E1M5 (E1M5)
61: E1M6 (E1M6)
72: E1M7 (E1M7)
83: E1M8 (E1M8)
(...)

```

Print all items inside specified map group:

```
$ ./wadls.py -g E1M1 doom1.wad

doom1.wad:     IWAD; numlumps: 2045; infotableofs: 0x009e2e84
doom1.wad: Selected 10 lumps from 'E1M1' group:
[   7]:     THINGS     1380 at 0x0001f878
[   8]:   LINEDEFS     6650 at 0x0001fddc
[   9]:   SIDEDEFS    19440 at 0x000217d8
[  10]:   VERTEXES     1868 at 0x000263c8
(...)
```

Print all items inside specified group of sprites/floors:

```
$ ./wadls.py -g S doom1.wad

doom1.wad:     IWAD; numlumps: 2045; infotableofs: 0x009e2e84
doom1.wad: Selected 753 lumps from 'S' group:
[ 820]:     CHGGA0     8180 at 0x004cba00
[ 821]:     CHGGB0     8204 at 0x004cd9f4
(...)
```

Collecting many wad files into one:

```
$ ./wadcat.py output.wad a.wad b.wad c.wad
```
