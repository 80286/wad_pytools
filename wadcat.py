#!/usr/bin/python3
# -*- coding:utf-8 -*-
# 27.09.15, 80286

from wad import Wad
import getopt
import sys
import os


class WadCatCfg(object):
    @staticmethod
    def printUsage(argv):
        print("Build wad file from many different files.")
        print("Usage: {0} [options] <output.wad> <file1.wad> <file2.png> [./test.txt:FILE3] ...".format(argv[0]))
        print("-v, --verbose: Enable verbose mode.")
        sys.exit(1)

    def __init__(self):
        self.verboseMode = False
    
    def buildFromArgv(self, argv):
        shortOpts = "hv"
        longOpts = ["help", "verbose"]

        if len(argv) < 3:
            self.printUsage(argv)

        try:
            (opts, self.args) = getopt.getopt(argv[1:], shortOpts, longOpts)
        except getopt.GetoptError as e:
            print(str(e))
            self.printUsage(argv)

        for (key, _) in opts:
            if key in ("-h", "--help"):
                self.printUsage(argv)
            elif key in ("-v", "--verbose"):
                self.verboseMode = True


class WadCat(object):
    @staticmethod
    def main(argv):
        cfg = WadCatCfg()
        cfg.buildFromArgv(argv)

        outputFilename = cfg.args[0]
        outputWad = Wad(outputFilename)
        outputWad.verbose = cfg.verboseMode

        for arg in cfg.args[1:]:
            cmdArgs = arg.split(":")
            inFilename = cmdArgs[0]
            lumpName = cmdArgs[1] if len(cmdArgs) == 2 else None
            extension = inFilename[-3:]

            if extension.lower() != "wad":
                try:
                    if os.path.isdir(inFilename):
                        outputWad.appendDir(inFilename)
                    else:
                        outputWad.appendFile(inFilename, lumpName)
                except IOError as e:
                    print(str(e))
                continue
            elif lumpName is not None:
                print("Lump name '{0}' ignored for '{1}' file.".format(lumpName, inFilename))

            try:
                part = Wad(inFilename)
                part.verbose = cfg.verboseMode
                part.read()
                part.close()
            except IOError as e:
                print(str(e))
                continue

            outputWad.appendWad(part)

        numOfWritenBytes = outputWad.build()
        print("Saved {0} entries in wad file '{1}' ({2} bytes).".format(outputWad.getDirectoryLength(), outputFilename, numOfWritenBytes))


if __name__ == '__main__':
    WadCat.main(sys.argv)
