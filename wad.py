#!/usr/bin/python3
# -*- coding:utf-8 -*-
# 27.09.15, 80286

import getopt
import struct
import sys
import os
import re


class WadConstants(object):
    # Format of wadinfo_t struct:
    wadInfoFmt = "4sii"
    # Format of filelump_t struct:
    dirLumpFmt = "ii8s"

    markerStart = "START"
    markerEnd = "END"
    markerFmt = "{0}_{1}"

    # Formats of map files structures:
    # "NAME": ("struct_format", bytes, ("Field1 title", ... ))
    mapLumps = {
        "THINGS":           ("hhhhh", 10, ("X", "Y", "Angle", "Type", "Flags")),
        "THINGS_Hexen":     ("hhhhhhhbbbbbb", 20, ("Id", "X", "Y", "Z", "Angle", "Type", "Flags", "Specials")),
        "LINEDEFS":         ("HHHHHhh", 14, ("Start", "End", "Flags", "Special", "Sector", "Right Sdef", "Left Sdef")),
        "LINEDEFS_Hexen":   ("HHHbbbbbbHH", 16, ("Start", "End", "Flags", "Special", "Arg1", "Arg2", "Arg3", "Arg4", "Arg5", "Right Sdef", "Left Sdef")),
        "VERTEXES":         ("hh", 4, ("X....", "Y....")),
        "SIDEDEFS":         ("hh8s8s8sh", 30, ("XOffset", "YOffset", "Upper", "Lower", "Middle", "Sector")),
        "SECTORS":          ("hh8s8shhh", 26, ("Floor", "Ceiling", "Floor Tex", "Ceiling Tex", "Light", "Type", "Tag")),
        "SEGS":             ("hhhhhh", 12, ("Start", "End", "Angle", "Linedef", "Direction", "Offset")),
        "SSECTORS":         ("hh", 4, ("Count", "Start seg")),
        "NODES":            ("hhhhhhhhhhhhhh", 28,
                             ("PartX", "PartY", "ChX", "ChY",
                              "L:T", "L:B", "L:L", "L:R",
                              "R:T", "R:B", "R:L", "R:R",
                              "Left", "Right")),
        "REJECT":            None,
        "BLOCKMAP":          None,
        "BEHAVIOR":          None
    }

    # Format of maptexture_t struct:
    mapTextureFmt = ("8sihhih", 22, ("Name", "Masked", "Width", "Height", "CDirectory", "PatchCount"))

    # Format of mappatch_t struct:
    mapPatchFmt = ("hhhhh", 10, ("OriginX", "OriginY", "Patch", "Stepdir", "Colormap"))

    # Doom I  map pattern:
    d1map = re.compile(r"E\dM\d")
    # Doom II map pattern:
    d2map = re.compile(r"MAP\d{2}")


class Struct(object):
    @staticmethod
    def strToBytes(s):
        return bytes(s, encoding="utf-8")

    @staticmethod
    def bytesToStr(b):
        try:
            return b.decode("utf-8")
        except Exception as e:
            print("Input: ", b)
            raise e

    @staticmethod
    def unpackFromFile(fmt, fileObj):
        fileData = fileObj.read(struct.calcsize(fmt))
        return struct.unpack(fmt, fileData)

    @staticmethod
    def unpack(fmt, data):
        return struct.unpack(fmt, data)


class Filelump(object):
    offset = 0
    name = ""
    # Size in bytes:
    size = 0
    data = None
    index = -1

    @staticmethod
    def fixLumpName(name):
        name = name.upper()
        nameLen = len(name)

        if nameLen > 8:
            return name[:8]
        elif nameLen == 8:
            return name
        else:
            return name.ljust(8, "\0")

    def getZeroPaddedName(self):
        return self.fixLumpName(self.name)

    def getZeroStrippedName(self):
        return self.name.rstrip("\0")

    def __init__(self, lump):
        self.offset = lump[0]
        self.size = lump[1]
        self.name = lump[2]

    def read(self, f):
        """ Reads data (lump.size bytes) from file f located at lump.offset.

            @param f
                File object.
        """
        f.seek(self.offset)
        self.data = f.read(self.size)

    def isMapName(self):
        name = self.getZeroStrippedName()
        # print(name, len(name), self.name, len(self.name))
        return WadConstants.d2map.match(name) or WadConstants.d1map.match(name)

    def isMapLump(self):
        name = self.getZeroStrippedName()
        return name in WadConstants.mapLumps

    def isPatchesLump(self):
        name = self.getZeroStrippedName()
        return name == "PNAMES"

    def isTextureLump(self):
        name = self.getZeroStrippedName()
        return name in ("TEXTURE1", "TEXTURE2")

    def getMarkerInfo(self):
        """ Checks whether lumpName begins/ends
        group of lumps (X_START/X_END marker) or map files (MAPX/ExMy marker).

        @param lumpName
            Name to check.
        @return
            If lumpName has marker name format, returns list
            containing marker type and number indicating beginning (0)
            or closing group of files (1) e.g. ["SS", 0]. In other case
            returns None.
        """
        lumpName = self.getZeroStrippedName()

        if self.isMapName():
            return [lumpName, 2]

        sepIdx = lumpName.find("_")
        if sepIdx == -1:
            return None

        groupName = lumpName[:sepIdx].strip("\0")
        markerType = lumpName[sepIdx + 1:].strip("\0")
        
        if markerType == WadConstants.markerStart:
            markerType = 0
        elif markerType == WadConstants.markerEnd:
            markerType = 1
        else:
            return None

        if len(groupName) == 0:
            return None

        return (groupName, markerType)

    def __str__(self):
        if self.index == -1:
            return "%8s: %5d bytes at 0x%08x" % (self.name, self.size, self.offset)
        else:
            return "[%4d]: %8s: %5d bytes at 0x%08x" % (self.index, self.name, self.size, self.offset)


class Wad(object):
    @staticmethod
    def buildStartMarkerId(markerName):
        return WadConstants.markerFmt.format(markerName, WadConstants.markerStart)

    @staticmethod
    def buildEndMarkerId(markerName):
        return WadConstants.markerFmt.format(markerName, WadConstants.markerEnd)

    @staticmethod
    def _convertFilenameToWadLumpName(path):
        sep = path.rfind("/") + 1

        if sep == 1:
            return path
        elif path[sep - 2] == "\\":
            p = path

            # Found slash with escape
            while True:
                p = p[:sep]
                sep = p.rfind("/")
                if sep == -1:
                    return path
                elif sep != 0 and p[sep - 1] != "\\":
                    return path[sep + 1:]
                elif sep == 0:
                    return path[1:]
                else:
                    continue
        else:
            # Cut path to basename
            return path[sep:]

    @staticmethod
    def _fixPath(path):
        sep = "/"
        return path.rstrip(sep) + sep

    def __str__(self):
        return "{0} '{1}' with {2} lumps.".format(self.info[0], self.filename, len(self.directory))

    def __init__(self, filename):
        # Relative/absolute path to wad file:
        self.filename = filename

        # Shortened name without path separators:
        self.filenameBase = self._convertFilenameToWadLumpName(self.filename)
        self.directory = []
        self.info = ['PWAD', 0, 0]
        self.fh = None
        self.verbose = False

    def isIWAD(self):
        return self.info[0][0] == 'I'

    def isPWAD(self):
        return self.info[0][0] == 'P'

    def getMapLumpDataList(self, lump):
        """ Retrieves list of records contained in data of lump.

        @param lump
            Map Filelump.
        @return
            Tuple (list, fmt) with list of records and boolean variable
            containing information about lump format.
        """
        data = lump.data
        mapLumps = WadConstants.mapLumps

        if lump.name not in mapLumps:
            return None

        if not mapLumps[lump.name]:
            return (data, False)

        mapLumpFmt = mapLumps[lump.name][0]
        mapLumpSize = mapLumps[lump.name][1]
        hexenFmt = False
        msgSize = "{0}: Wrong size of lump {1}.".format(self.filenameBase, lump.name)
        arrayLen = 0

        if lump.size % mapLumpSize == 0:
            arrayLen = lump.size / mapLumpSize
        else:
            # Lump size is not divisible by mapLumpSize,
            # try size of Hexen format:
            if lump.name in ("THINGS", "LINEDEFS"):
                mapLumpSize = mapLumps[lump.name + "_Hexen"][1]

                if lump.size % mapLumpSize == 0:
                    hexenFmt = True
                else:
                    print(msgSize)
                    return None
            else:
                print(msgSize)
                return None

        # print("mapLumpSize: %d; hexenFmt: %s" %(mapLumpSize, "Y" if hexenFmt else "N"))

        offset = 0
        result = []
        for _ in range(arrayLen):
            chunk = data[offset:offset + mapLumpSize]
            result.append(Struct.unpack(mapLumpFmt, chunk))
            offset += mapLumpSize

        return (result, hexenFmt)

    @staticmethod
    def getPatchNames(lump):
        """ Retrieves list with names of patches from lump.

        @param
            PNAMES lump.
        @return
            List of strings.
        """
        data = lump.data
        result = []
        offset = 0
        # Extract number of patches:
        patchesLen = Struct.unpack("i", data[:4])[0]
        offset = 4

        for _ in range(patchesLen):
            chunk = data[offset:offset + 8].rstrip("\0")
            result.append(chunk)
            offset += 8

        return result

    @staticmethod
    def getTextures(lump):
        """ Retrieves list with definitions of textures from lump.

        @param lump
            TEXTURE1/TEXTURE2 lump.
        @return
            List of tuples (texInfo, patches), where patches
            contains list of mappatch_t structures (tuples).
        """
        textures = []

        mapTexFmt = WadConstants.mapTextureFmt
        mapPatchFmt = WadConstants.mapPatchFmt

        data = lump.data
        numOfTextures = Struct.unpack("i", data[:4])[0]
        offset = 4 * (numOfTextures + 1)

        for _ in range(numOfTextures):
            texData = data[offset:offset + mapTexFmt[1]]
            texInfo = Struct.unpack(mapTexFmt[0], texData)
            offset += mapTexFmt[1]
            numOfPatches = texInfo[5]
            patches = []

            for _ in range(numOfPatches):
                patchData = data[offset:offset + mapPatchFmt[1]]
                patch = Struct.unpack(mapPatchFmt[0], patchData)
                patches.append(patch)
                offset += mapPatchFmt[1]

            textures.append((texInfo, patches))

        return textures

    @staticmethod
    def getPalette(lump, paletteIdx=0):
        """ Retrieves array of bytes inside PLAYPAL lump.

        @param lump
            PLAYPAL lump.
        @param paletteIdx
            Index of palette.
        @return
            List of 768 integers (256 * (r, g, b) colors).
        """
        data = lump.data
        offset = 768 * paletteIdx
        palette = []

        for _ in range(256):
            color = Struct.unpack("hhh", data[offset:offset + 3])
            palette.append(color[0])
            palette.append(color[1])
            palette.append(color[2])
            offset += 3

        return palette

    def getOtherLumpData(self, lump):
        lumps = {
            "PNAMES": self.getPatchNames,
            "TEXTURE1": self.getTextures,
            "TEXTURE2": self.getTextures,
            "PLAYPAL": self.getPalette
        }

        return lumps[lump.name](lump) if lump.name in lumps else None

    def getDirectoryLength(self):
        return len(self.directory)

    def _printMsg(self, msg):
        if not self.verbose:
            return

        print(msg)

    def appendMarker(self, name):
        self._printMsg("Creating marker '{0}'...".format(name))
        lump = Filelump((-1, 0, name))
        lump.data = ""
        self.directory.append(lump)

    def startGroup(self, name):
        self.appendMarker(Wad.buildStartMarkerId(name))

    def endGroup(self, name):
        self.appendMarker(Wad.buildEndMarkerId(name))

    def appendFile(self, filename, lumpName=None):
        data = None
        size = None

        with open(filename, "rb") as f:
            data = f.read()
            size = f.tell()

        lumpName = Filelump.fixLumpName(lumpName if lumpName else self._convertFilenameToWadLumpName(filename))

        lump = Filelump((-1, size, lumpName))
        lump.data = data
        self.directory.append(lump)

    def appendWad(self, wad):
        """ Appends content of other WAD file to WAD.

        @param wad
            Other instance of WAD file.
        """
        self._printMsg("{0}: Appending {1} lumps from {2}...".format(self.filenameBase, len(wad.directory), wad.filename))
        size = 0

        for lump in wad.directory:
            self.directory.append(lump)
            size += lump.size

        self._printMsg("{0}: Appended {1} bytes.".format(self.filenameBase, size))

    def appendDir(self, dirPath):
        for root, _, files in os.walk(dirPath):
            isDir = (root != dirPath)
            markerPrefix = ""

            if isDir:
                markerPrefix = os.path.basename(root)
                self.startGroup(markerPrefix)

            for f in files:
                fileToAppend = os.path.join(root, f)
                self.appendFile(fileToAppend)

                if markerPrefix:
                    msg = "File '{0}' added to '{1}' group.".format(fileToAppend, markerPrefix)
                else:
                    msg = "File '{0}' added.".format(fileToAppend)
                
                self._printMsg(msg)

            if isDir:
                self.endGroup(markerPrefix)

    def build(self, outfile=None):
        """ Builds new WAD file using current directory data.

        @param outfile
            Name of new WAD file.
        """
        if outfile is None:
            outfile = self.filename

        self._printMsg("Building '{0}' file...".format(outfile))

        with open(outfile, "wb") as f:
            # Skip over header:
            offset = 12
            f.seek(offset)
            lumpOffsets = []

            # Write directory data:
            for lump in self.directory:
                lump.offset = offset
                lumpOffsets.append(offset)
                f.write(lump.data)
                offset += lump.size

            infotableofs = f.tell()
            # Write directory indices:
            for i, lump in enumerate(self.directory):
                lumpNameBytes = lump.getZeroPaddedName().encode("ascii")
                lumpStr = struct.pack(WadConstants.dirLumpFmt, lumpOffsets[i], lump.size, lumpNameBytes)
                f.write(lumpStr)

            f.seek(0)
            infoStr = struct.pack(WadConstants.wadInfoFmt, self.info[0].encode("ascii"), len(self.directory), infotableofs)
            f.write(infoStr)

            self._printMsg("Saved {0} bytes.".format(offset))

        return offset

    def readDirectoryData(self):
        """ Reads data associated with directory lumps.
        """
        self._printMsg("{0}: Reading directory data from {1} lumps...".format(self.filenameBase, self.info[1]))
        size = 0

        for lump in self.directory:
            lump.read(self.fh)
            size += lump.size

        self._printMsg("{0}: Reading of {1} bytes has finished.".format(self.filenameBase, size))

    def readDirectoryByFileObject(self, f, dirOffset, dirLen, readData):
        """ Reads WAD file directory array.

        @param f
            File object.
        @param dirOffset
            Beginning of directory array in file.
        @param dirLen
            Number of directory lumps to read.
        @param readData
            If True, reads data associated with lumps.
        @return
            List of directory lumps.
        """
        wadDir = []
        f.seek(dirOffset)
        size = 0

        for i in range(dirLen):
            lumpTuple = Struct.unpackFromFile(WadConstants.dirLumpFmt, f)
            lump = Filelump(lumpTuple)
            lump.name = Struct.bytesToStr(lump.name)
            lump.index = i
            prevOffset = f.tell()

            if readData:
                lump.read(f)
                size += lump.size

            f.seek(prevOffset)
            wadDir.append(lump)

        if readData:
            self._printMsg("Reading of {0} bytes has finished.".format(size))

        return wadDir

    def readHeader(self, f):
        """ Returns header of WAD contained in first 12 bytes of WAD file.

        @param
            File object.
        @return
            Tuple (wadType, numlumps, infotableofs).
        """
        f.seek(0)
        info = list(Struct.unpackFromFile(WadConstants.wadInfoFmt, f))
        info[0] = Struct.bytesToStr(info[0])
        wadType = info[0]

        if (wadType[0] not in ("I", "P")) or (wadType[1:] != "WAD"):
            raise BaseException("'{0}' is not valid WAD file. Found id: '{1}'".format(self.filenameBase, wadType))

        return info

    def readLump(self, lump):
        """ Fills lump object with lump.size bytes located at lump.offset position
        in WAD file.

        @param lump
            Lump to update.
        """
        lump.read(self.fh)

    def read(self, readData=True):
        """ Reads content of WAD file.

            @param readData
                If True, function will read directory indices and data associated with them.
                In other case it will read only indices.
        """
        try:
            self.fh = open(self.filename, "rb")
        except IOError as e:
            print(str(e))
            return

        self.info = self.readHeader(self.fh)
        self.directory = self.readDirectoryByFileObject(self.fh, self.info[2], self.info[1], readData)

    def getGroup(self, groupName):
        """ Returns list of indices contained in given group.

        @param groupName
            Name of a group.
        """
        groupStack = []
        groupItems = []
        groupMap = False

        for lump in self.directory:
            group = lump.getMarkerInfo()

            if group is not None:
                if group[1] in (0, 2):
                    groupStack.append(group[0])

                    if group[1] == 2:
                        groupMap = True
                else:
                    if group[0] == groupStack[-1]:
                        groupStack.pop()
                    else:
                        self._printMsg("{0}: Found mismatched marker '{1}'.".format(self.filenameBase, lump.name))
                continue

            if groupMap and not lump.isMapLump():
                groupMap = False
                groupStack.pop()

            if groupStack and groupStack[-1] == groupName:
                groupItems.append(lump)

        return groupItems

    def extractRawData(self, path, pattern=None):
        """ Extracts content of WAD file to given place.

            @param path
                Destiny of WAD content.
            @param pattern
                Pattern defining group to extract.
        """
        groupMap = False
        groupDir = ""
        groupStack = []

        fixedPath = self._fixPath(path)
        compiledPattern = re.compile(pattern) if pattern else None

        for lump in self.directory:
            self.fh.seek(lump.offset)

            if compiledPattern and (not compiledPattern.match(lump.name)):
                continue

            group = lump.getMarkerInfo()

            if group is not None:
                # If lumpName is map marker or START marker:
                if group[1] in (0, 2):
                    self._printMsg("{0}: Found beginning of group {1}.".format(self.filenameBase, group[0]))

                    groupStack.append(group[0])
                    if group[1] == 2:
                        groupMap = True

                    # Build directory tree for groups:
                    groupDir = "/".join(groupStack) + "/"
                    self._printMsg("{0}: Saving '{1}' group data to {2} directory.".format(self.filenameBase, lump.name, groupDir))

                    groupDirName = fixedPath + groupDir
                    try:
                        # Create directory for group:
                        os.mkdir(groupDirName)
                    except OSError as e:
                        if e.errno == 17:
                            self._printMsg("Directory {0} already exists.".format(groupDirName))
                        else:
                            print(str(e))
                            return
                else:
                    if group[0] == groupStack[-1]:
                        groupStack.pop()
                        self._printMsg("{0}: Found end of group {1}.".format(self.filenameBase, group[0]))
                        groupDir = ""
                    else:
                        self._printMsg("{0}: Found mismatched marker '{1}'.".format(self.filenameBase, lump.name))

                # Skip saving of map marker.
                continue

            # If there is no more map lumps:
            if groupMap and (lump.name not in WadConstants.mapLumps):
                groupMap = False
                groupDir = ""
                groupStack.pop()

            # Select output filename:
            lumpOutfile = fixedPath + groupDir + lump.name

            self._printMsg("{0}: Extracting data from {1} lump to file {2}...".format(self.filenameBase, lump.name, lumpOutfile))

            # Save lump content to file:
            with open(lumpOutfile, "wb") as f:
                f.write(self.fh.read(lump.size))

    def close(self):
        if self.fh:
            self.fh.close()

    @staticmethod
    def main(argv):
        argv = sys.argv

        cfg = WadCfg()
        cfg.buildFromArgv(argv)

        if len(cfg.args) == 0:
            cfg.printUsage(argv)

        for arg in cfg.args:
            wad = Wad(arg)
            wad.verbose = cfg.verboseMode
            wad.read()

            if cfg.extractDirectory:
                wad.extractRawData(cfg.extractDirectory, cfg.extractPattern)

            wad.close()


class WadCfg(object):
    def __init__(self):
        self.extractDirectory = None
        self.extractPattern = None
        self.verboseMode = False
        self.args = []

    @staticmethod
    def printUsage(argv):
        print("Usage: {0} [options] <file.wad>".format(argv[0]))
        print("Extract data from WAD file.")
        print("-x, --extract <path_to_output_directory>: Destination of extracted files.")
        # print("-o, --output <newfile.wad>: Build WAD file.")
        print("-v, --verbose: Enable verbose mode.")
        print("-e, --regexp <pattern>: Extract files with names match to the given pattern (extended regex)")
        sys.exit(1)

    def buildFromArgv(self, argv):
        shortOpts = "hx:o:e:v"
        longOpts = ["help", "extract=", "output=", "regexp=", "verbose"]
        argv = sys.argv

        try:
            (opts, self.args) = getopt.getopt(argv[1:], shortOpts, longOpts)
        except getopt.GetoptError as e:
            print(str(e))
            self.printUsage(argv)

        for (opt, val) in opts:
            if opt in ("-x", "--extract"):
                self.extractDirectory = val
            elif opt in ("-h", "--help"):
                self.printUsage(argv)
            elif opt in ("-e", "--regexp="):
                self.extractPattern = val
            elif opt in ("-o", "--output="):
                pass
            elif opt in ("-v", "--verbose"):
                self.verboseMode = True
            else:
                assert False, "Unhandled option"


if __name__ == '__main__':
    Wad.main(sys.argv)
